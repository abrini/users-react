import React from 'react';

export default class User extends React.Component {
  render() {
    return(
      <p>
        {`${this.props.name} - ${this.props.username}`}
      </p>
    );
  }
}
