import React from 'react';

import Users from './users';

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      users: []
    };
  }

  componentDidMount() {
    let _this = this;
    fetch('https://brinidesigner.com/test/users.json')
    .then(function(response) {
      return response.json()
    }).then(function(json) {
      _this.setState({
        users: json
      });
    }).catch(function(ex) {
      console.error('parsing failed', ex)
    });
  }

  clearUsers = () => {
    this.setState({
      users: []
    });
  }

  render() {
    return(
      <section>
        <button onClick={ this.clearUsers }>Clear list</button>
        <Users data={ this.state.users } />
      </section>
    );
  }
}
