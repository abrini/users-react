import React from 'react';

import User from './user';

export default class Users extends React.Component {

  eachUser(user, i) {
    return(
      <User name={ user.name } username={ user.username } key={ i } />
    )
  }

  render() {
    return(
      <div>
        { this.props.data.map(this.eachUser) }
      </div>
    );
  }
}
