Data URL: https://brinidesigner.com/test/users.json

Fetch library for making ajax calls: https://github.com/github/fetch

3 ways to bind this to a method
* while calling the event: this.clearUsers.bind(this) : this works but it's not a good practice
* in the constructor: this.clearUsers = this.clearUsers.bind(this)
* using arrow functions in the function definition: clearUsers = () => {}